export EDITOR='vim'
#export GEM_HOME=~/.gem
#export GEM_PATH=~/.gem
alias tm="tmux attach -t my-session || tmux new -s my-session"
alias ping='prettyping --nolegend'
alias du="ncdu --color dark -rr -x --exclude .git --exclude node_modules"


export PROMPT_COMMAND='if [ "$(id -u)" -ne 0 ]; then echo "$(date "+%Y-%m-%d.%H:%M:%S") $(pwd) $(history 1)" >> ~/.logs/bash-history-$(date "+%Y-%m-%d").log; fi'
# export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND ;} history -a"
export PATH="/usr/local/bin:/usr/local/sbin:~/bin:/Users/master/tools:/Applications/iTerm.app/Contents/MacOS:/Users/master/work/platform-tools:/usr/local/opt/openssl/bin:/Applications/Visual Studio Code.app/Contents/Resources/app/bin:$PATH"

if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
    export POWERLINE_BIN="$HOME/.local/bin"
    export POWERLINE_BINDINGS="$HOME/.local/lib/python2.7/site-packages/powerline/bindings"
fi

if [ -d "$HOME/Library/Python/2.7/bin" ]; then
    PATH="$HOME/Library/Python/2.7/bin:$PATH"
    export POWERLINE_BIN="$HOME/Library/Python/2.7/bin"
    export POWERLINE_BINDINGS="$HOME/Library/Python/2.7/lib/python/site-packages/powerline/bindings"
fi

export POWERLINE_COMMAND=$POWERLINE_BIN/powerline
export POWERLINE_CONFIG_COMMAND=$POWERLINE_BIN/powerline-config

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. $POWERLINE_BINDINGS/bash/powerline.sh

[ -s "/Users/master/.scm_breeze/scm_breeze.sh" ] && source "/Users/master/.scm_breeze/scm_breeze.sh"

alias preview="fzf --preview 'bat --color \"always\" {}'"
#alias preview="fzf --height 40% --preview 'if file -i {}|grep -q binary; then file -b {}; else bat --color \"always\" --line-range :40 {}; fi'"
# add support for ctrl+o to open selected file in VS Code
export FZF_DEFAULT_OPTS="--bind='ctrl-o:execute(code {})+abort'"

# extract any time of compressed file
function extract {
    echo Extracting $1 ...
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xjf $1  ;;
            *.tar.gz)    tar xzf $1  ;;
            *.bz2)       bunzip2 $1  ;;
            *.rar)       rar x $1    ;;
            *.gz)        gunzip $1   ;;
            *.tar)       tar xf $1   ;;
            *.tbz2)      tar xjf $1  ;;
            *.tgz)       tar xzf $1  ;;
            *.zip)       unzip $1   ;;
            *.Z)         uncompress $1  ;;
            *.7z)        7z x $1  ;;
            *)        echo "'$1' cannot be extracted via extract()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

function _t() {
  # Defaults to 3 levels deep, do more with `t 5` or `t 1`
  # pass additional args after
  local levels=${1:-3}; shift
  tree -I '.git|node_modules|bower_components|.DS_Store' --dirsfirst -L $levels -aC $@
}
alias t=_t
