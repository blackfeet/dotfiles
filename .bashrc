[ -f ~/.fzf.bash ] && source ~/.fzf.bash

[ -s "/Users/master/.scm_breeze/scm_breeze.sh" ] && source "/Users/master/.scm_breeze/scm_breeze.sh"

[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh
